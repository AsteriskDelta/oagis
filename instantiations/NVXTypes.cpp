#include "iOagis.h"

namespace nvx {
    template class NI2<48,16,0>;
    template class NVX2<3, ::Oagis::Num>;
    template class NVX2<2, ::Oagis::Num>;
    template class Quaternion<::Oagis::Vec3>;
};
