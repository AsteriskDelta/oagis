#ifndef OAGIS_CONTOUR_H
#define OAGIS_CONTOUR_H
#include "iOagis.h"
#include "Node.h"
#include <vector>

namespace Oagis {
    class ContourNode : public Node {
    public:
        ContourNode();
        virtual ~ContourNode();
        
        Polyline3D path;
    protected:
        
    };
};

#endif

