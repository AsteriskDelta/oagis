#ifndef OAGIS_INC_H
#define OAGIS_INC_H
#include <iostream>
#include <Spinster/Spinster.h>
#include <NVX/NVX.h>
#include <NVX/Spatial.h>
#include <Spinster/Defer.h>

namespace Oagis {
    constexpr const uint8_t FoldExp = 2;
    
    namespace $ = ::spin;
    using namespace ::nvx;
    typedef NI2<48,16,0> Num;
    typedef NVX2<3, Num> Vec3;
    typedef NVX2<2, Num> Vec2;
    typedef Quaternion<Vec3> Quat;
    typedef uint64_t Idt_t;
    
    typedef ::nvx::Transform<Vec3> Transform;
    typedef ::nvx::AlignedTransform<Vec3> AlignedTransform;
    
    //Index
    class Object;
    class Container;
    class Node;
    class Index;
    class Iterator;
    
    //Subspace
    
    //Material
    class Material;
    
    //Contour
    
    //Voxel
    
    //Vortex
    
    
};

namespace Spinster {
    template<>
    struct Resolver<Oagis::Object> {
        static Defer<Oagis::Object> Execute(const uint64_t id);
        static uint64_t Release(Defer<Oagis::Object> obj);
        static Defer<Oagis::Object> Reserve(Defer<Oagis::Object> obj);
    };
};


namespace Oagis {
    //Pointers
    typedef $::Defer<Object> ObjectPtr;
    typedef $::Defer<Object> ContainerPtr;
    typedef $::Defer<Material> MaterialPtr;
};

#include "Generic.h"

#endif
