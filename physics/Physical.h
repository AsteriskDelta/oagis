#ifndef OAGIS_PHYSICAL_H
#define OAGIS_PHYSICAL_H
#include "iOagis.h"
#include "Object.h"

namespace Oagis {
    
    class PhysicalObject : public Generic, public Object {
    public:
        PhysicalObject();
        ~PhysicalObject();
        
        Vec3 velocity;
        Quat angular;
        
        struct Center {
            Vec3 mass, pressure;
        } center;
    };
    
};

#endif
