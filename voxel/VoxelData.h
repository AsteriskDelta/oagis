#ifndef OAGIS_VOXEL_DATA_H
#define OAGIS_VOXEL_DATA_H
#include "iOagis.h"

namespace Oagis {
    
    struct VoxelData {
        MaterialPtr material;
        //Num fill;
        //Num curve;
        Vec3 normal;
        Num extent, curve;
        Num volume() const;
    }
}

#endif
