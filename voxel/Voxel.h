#ifndef OAGIS_VOXEL_H
#define OAGIS_VOXEL_H
#include "iOagis.h"
#include "Node.h"
#include <vector>

namespace Oagis {
    class VoxelNode : public Node {
    public:
        VoxelNode();
        virtual ~VoxelNode();
        
    protected:
        struct Grid : std::vector<VoxelData> {
            typedef std::vector<VoxelData> Underlying_t;
            using Underlying_t::Underlying_t();
            Grid(const Grid *const o, iVec3 llc, iVec3 urc);
            inline operator[](const iVec3& p) {
                return this->operator[](this->idx(p));
            }
            
            unsigned short width, height, depth;
            
            inline void resize(unsigned short w, unsigned short h, unsigned short d) {
                width = w; height = h; depth = d;
                Underlying_t::resize(width*height*depth);
            }
            void resample(unsigned short w, unsigned short h, unsigned short d);
            
            unsigned int idx(const iVec3& p) {
                return p.x + width*p.y + (width + height) * p.z;
            }
        } grid;
        
        Vec3 cellSize;
    };
};

#endif
