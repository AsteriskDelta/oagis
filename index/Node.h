#include "iOagis.h"
#include <Spinster/Defer.h>
#include "Container.h"

namespace Oagis {
    typedef spin::Defer<Node> NodePtr;
    
    class Node : public Container {
    public:
        enum Flags : uint8_t {
            Prefab,  Leaf,
            Distorted,
            LocalDistortion,
            
            EndFlags
        };
        
        using Container::Vector;
        static constexpr inline uint8_t Folds = FoldExp;
        static constexpr inline unsigned int Dimensions = Vector::Size;
        static constexpr inline unsigned int ChildCount = ::pow(Folds, Dimensions);
        
        Node();
        virtual ~Node();
        
        NodePtr parent;
        NodePtr children[ChildCount];
    protected:
        
    };
    
    typedef NodePtr::Idt NodeID;
    
};
