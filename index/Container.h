#ifndef OAGIS_CONTAINER_H
#define OAGIS_CONTAINER_H
#include "iOagis.h"
#include "Object.h"
#include <NVX/Bounded.h>
#include <list>

namespace Oagis {
    class Container : public Object, public Bounded<Vec3> {
    public:
        using Object::Vector;
        
        //Object "owned" by this container
        std::list<ObjectPtr> objects;
        //Objects owned by another container, but still occupying this one spatially
        std::list<ObjectPtr> occupants;
    };
};

#endif
