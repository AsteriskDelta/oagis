#ifndef OAGIS_OCCUPANT_H
#define OAGIS_OCCUPANT_H
#include "iOagis.h"
#include <NVX/Transform.h>

namespace Oagis {
    
    class Object : public Generic, public Transform {
    public:
        typedef Vec3 Vector;
        
        Object();
        virtual ~Object();
        
        Idt_t id() const;
        
        _generic(Object, object);
        
        
    };
};

#endif
