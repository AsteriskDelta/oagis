#ifndef OAGIS_GENERIC_H
#define OAGIS_GENERIC_H

#define PROXY_PTR(OBJ, VAR) \
    inline virtual OBJ##Ptr VAR() const;
#define _generic(OBJ, VAR) \
    inline virtual OBJ##Ptr VAR() const override {\
        return static_cast<OBJ##Ptr>(const_cast<Object*>(reinterpret_cast<const Object*>(this)));\
    }

namespace Oagis {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wnon-virtual-dtor"
    class Generic {
    public:
        Generic();
        virtual ~Generic();
        
        PROXY_PTR(Object, object);
        PROXY_PTR(Container, container);
    };
#pragma GCC diagnostic pop
};
#endif
